# SimpleTM 
> Simple Task Manager    

SimpleTM is small task manager with messageing system written in C++.
You can also:

## Build and run example 

### 1. Dependencies
SimpleTM uses Poco and standard library.
1. Download Poco lib from this [link](http://pocoproject.org/releases/poco-1.7.4/poco-1.7.4.tar.gz)
2. Extract it and cd to it. 
3. run next commands: 
```bash 
$ ./configure
$ ./make  
```
4. create symbolic link to project_root_folder/lib
```bash
$ ln -s /path/to/poco/poco-1.7.4 /path/to/project_root_folder/lib/poco
```

### 2. Create MakeFiles
SimpleTM uses CMake for generation of makefiles. 
Run CMake with following bash commands:
```
$CMake -G "Unix Makefiles"
```

###3. Compile 
```bash 
$make -j[number_of_physical_cores+1] 
```

###4. Run 
Example executable is stored in project sub folder bin/. Cd in to it and run example with command:
```bash 
$./TaskMngr 
```

