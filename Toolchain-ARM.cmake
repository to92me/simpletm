#Cmake configurations 
SET(CMAKE_SYSTEM_NAME Linux)
SET(CMAKE_SYSTEM_VERSION 1)

#set c and c++ compiler 
SET(CMAKE_C_COMPILER ${RPI_TOOLS}-gcc)
SET(CMAKE_CXX_COMPILER ${RPI_TOOLS}-g++)

#set raspberry pi filesystem libs
#SET(CMAKE_FIND_ROOT_PATH /home/project/RPI/tools/)
SET(CMAKE_FIND_ROOT_PATH ${RPI_ROOTFS})
SET(CMAKE_SYSROOT ${RPI_ROOTFS})

#mozda zatreba mozda i ne nemam pojma :D ! 
#SET(TOOLCHAIN_INCLUDE /home/tools/rpi_crosscompile/rootfs/usr/include)
#SET(TOOLCHAIN_LIB /home/tools/rpi_crosscompile/rootfs/usr/lib)

#additional cmake configurations 
  set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
  set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
  set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
  set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)
