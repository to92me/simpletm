project(TaskMngr CXX C)
cmake_minimum_required(VERSION 2.8)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g -Wall -std=c++0x -fpermissive")
#set(CMAKE_LINKER_FLAGS  "${CMAKE_LINKER_FLAGS} -lrt" )
#set(CMAKE_C_FLAGS  "${CMAKE_C_FLAGS} -lrt" )


set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${PROJECT_SOURCE_DIR}/bin")
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${PROJECT_SOURCE_DIR}/bin/arch")
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${PROJECT_SOURCE_DIR}/bin/lib")

add_subdirectory(src)
