
#include <iostream>
#include <csignal>
#include <ostream>
#include <stdio.h>

#include "core/Core.h"
#include "iostream"
#include "message/Notification.h"

#include "tasks/ExampleTask1.h"
#include "tasks/ExampleTask2.h"


using namespace core;
using namespace std;
using namespace task;

core::Core* coreMngr;

void signalHandler(int sigNum){
    cout<<"Recieved STOP signal!"<<endl;
    coreMngr->stop();
}


int main(int argc, char **argv){

    // create instances of tasks
    ExampleTask1 e_task1;
    ExampleTask2 e_task2;

    // define singalHandler for stop signal
    signal(SIGINT,signalHandler);

    //instance new Core
    coreMngr = new Core();

    // add view to tasks
    coreMngr->AddTask(&e_task1);
    coreMngr->AddTask(&e_task2);

    //starting core
    coreMngr->start();

    //do init method in all added tasks
    coreMngr->initAllTasks();

    // start all added tasks
    coreMngr->startAllTasks();

    //send test commands and notifications
//    Notification* notification = new Notification("test_topic");
//    coreMngr->sendMessage(notification);


//    Command* command = new Command("test_command","MainView","testSender");
//    coreMngr->sendMessage(command);

    // join everything
    coreMngr->joinAllTasks();
    coreMngr->join();

    return 0;
}

