#ifndef EXAMPLE_TASK2_H
#define EXAMPLE_TASK2_H

#include "core/AbstractTask.h"
#include "tasks/messages/ExampleCommand.h"
#include "tasks/messages/ExampleNotification.h"
#include "Poco/Timestamp.h"

#include <sstream>

using Poco::Timestamp;
using namespace core;

namespace task {

class ExampleTask2: public AbstractTask{
public:
    ExampleTask2():AbstractTask("ExampleTask2"){}

    void init();
    void run();
    void notificationReceiver(Notification* _notification);
    void commandReceiver(Command* _command);

    void stop();

    void successCallback(CommandResponse* _message);
    void errorCallback(CommandResponse* _message);
    void progressCallback(CommandResponse* _message);

protected:
    bool should_stop = false;

    Timestamp time;


};


}

#endif
