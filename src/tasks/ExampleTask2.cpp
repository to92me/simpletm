#include "tasks/ExampleTask2.h"

namespace task{

void ExampleTask2::init(){
    this->registerCommand("example_command1",(commandCallback)&ExampleTask2::commandReceiver);
    this->subscribe("example_topic",(notificationCallback)&ExampleTask2::notificationReceiver);


}

void ExampleTask2::run(){
//    while (should_stop == false) {
//       this->thread.sleep(200);

//       ExampleCommand* command = new ExampleCommand();

//       time.update();

//       command->setDestination("ExampleTask1");
//       command->setName("example_command1");
//       command->setTime(time.epochTime());

//       sendMessage((Command*)command);


//    }
    ExampleCommand* command = new ExampleCommand();
    time.update();
    command->setDestination("ExampleTask1");
    command->setName("example_command1");

    debug("sending command with response callbacks");

    // TODO uraditi static cast i onda
    this->sendMessage(command, (messageCallback)&ExampleTask2::successCallback, (messageCallback)&ExampleTask2::errorCallback,(messageCallback)&ExampleTask2::progressCallback);


}

void ExampleTask2::stop(){
    this->should_stop = true;
}


void ExampleTask2::commandReceiver(Command *_command){

    error("got command");

}

void ExampleTask2::notificationReceiver(Notification *_notification){

    debug("got notification");
}

void ExampleTask2::successCallback(CommandResponse *_message){
    debug("GOT SUCCESS");
}

void ExampleTask2::errorCallback(CommandResponse *_message){
    debug("GOT ERROR");
}

void ExampleTask2::progressCallback(CommandResponse *_message){
    debug("GOT PROGRESS");
}

}
