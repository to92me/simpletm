#ifndef EXAMPLE_COMMAND_H
#define EXAMPLE_COMMAND_H

#include "message/Command.h"

using namespace core;

namespace task{

class ExampleCommand: public Command{

public:
    ExampleCommand(): Command("ExampleCommand","ExampleTask1"){}

    int getTime()const{ return this->time;}
    void setTime(const int _time){this->time = _time;}

private:
    int time;

};
}

#endif


