
#ifndef EXAMPLE_NOTIFICATION_H
#define EXAMPLE_NOTIFICATION_H

#include "message/Notification.h"

using namespace core;

namespace task{

class ExampleNotification: public Notification{
public:
    ExampleNotification():Notification("example_topic"){}

    int getTime()const {return this->time;}
    void setTime(const int _time) {this->time = _time;}

private:
    int time;

};

}

#endif

