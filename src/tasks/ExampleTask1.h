#ifndef EXAMPLE_TASK1_H
#define EXAMPLE_TASK1_H

#include "core/AbstractTask.h"
#include "tasks/messages/ExampleCommand.h"
#include "tasks/messages/ExampleNotification.h"
#include "Poco/Timestamp.h"

#include <sstream>

using Poco::Timestamp;
using namespace core;

namespace task {

class ExampleTask1: public AbstractTask{
public:
    ExampleTask1():AbstractTask("ExampleTask1"){}

    void init();
    void run();
    void notificationReceiver(Notification* _notification);
    void commandReceiver(Command* _command);

    void stop();

protected:
    bool should_stop = false;

    Timestamp time;

};


}

#endif
