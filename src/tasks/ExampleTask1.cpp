#include "tasks/ExampleTask1.h"

namespace task{

void ExampleTask1::init(){
    this->registerCommand("example_command1",(commandCallback)&ExampleTask1::commandReceiver);
    this->subscribe("example_topic",(notificationCallback)&ExampleTask1::notificationReceiver);


}

void ExampleTask1::run(){
    int i = 0;

    while (should_stop == false) {

        this->thread.sleep(100);
        i++;

        if(i > 10){
            i= 0;
            time.update();

            ExampleNotification* notification = new ExampleNotification();
            notification->setTopic("example_topic");
            notification->setTime(time.epochMicroseconds());

            sendMessage((Notification*)notification);
        }

    }
}

void ExampleTask1::stop(){
    this->should_stop = true;
}


void ExampleTask1::commandReceiver(Command *_command){

    //    ExampleCommand* command = (ExampleCommand*) _command;


    debug("got some command sending response: ");
    std::stringstream ss;
    debug("sending progress response first");
    sendResponse(_command, ResposeType::PROGRESS);
    debug("sending success response");
    sendResponse(_command, ResposeType::SUCCESS);
    debug("sending error response after success this should raise on warring !");
    sendResponse(_command, ResposeType::ERROR);

    //    ss << command->getTime();

    //    debug(ss.str());

}

void ExampleTask1::notificationReceiver(Notification *_notification){

    error("got notification");

}

}
