#ifndef NOTIFICATION_H
#define NOTIFICATION_H

#include "Message.h"

namespace core{

class Notification: public Message{
public:
    Notification(const std::string& _topic, const std::string& _sender): Message(MessageType::NOTIFICATION, _sender), topic(_topic){}
    Notification(const std::string& _topic): Message(MessageType::NOTIFICATION), topic(_topic){}
    Notification(Notification* _notification): Message(_notification), topic(_notification->topic){}

     virtual ~Notification(){}

    void setTopic(const std::string& _topic);
    std::string getTopic() const;

    virtual Message* clone(){return this;}

private:
    std::string topic;
};
} // end namespace

#endif
