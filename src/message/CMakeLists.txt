set(MSSG_SRC
	Notification.cpp
        Command.cpp
        Message.cpp
        StopCommand.cpp
        CommandResponse.cpp
#        ResponseApi.cpp
#        ResponseFactory.cpp
)


add_library(mssg ${MSSG_SRC})

target_link_libraries(mssg tasks core)

