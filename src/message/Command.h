#ifndef COMMAND_H
#define COMMAND_H

#include "Message.h"
#include "Poco/String.h"

// note copy of milan: idea of invalid ID ( first set ID to some INVALID ID that in this case will be -1 )


namespace core{

class Command: public Message /*,public ResponseApi*/{
public:

    static int INVALID_ID; // this will be -1

    Command(std::string _command_name, std::string _destination, std::string _sender):Message(MessageType::COMMAND, _sender),
        destination(_destination), name(_command_name), id(INVALID_ID){}

    Command(std::string _command_name, std::string _destination):Message(MessageType::COMMAND),destination(_destination), id(INVALID_ID){}

    Command(Command* _cmd):Message(_cmd),destination(_cmd->destination), name(_cmd->name), id(INVALID_ID){}

    void setDestination(const std::string& _destination);
    std::string getDestination() const;

    void setName(const std::string& _command_name);
    std::string getName() const;

    int getId() const{
        return this->id;
    }

    inline void setId(const int _id){
        this->id = _id;
    }

    virtual ~Command(){}

    virtual Message* clone(){return this;}



private:
    std::string destination;
    std::string name;
    int id;
//    ResponseFactory response_factory;


};


}   // end namespace

#endif
