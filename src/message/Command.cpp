#include "Command.h"

namespace core{

int Command::INVALID_ID = -1;

std::string Command::getDestination()const{
    return this->destination;
}

void Command::setDestination(const std::string& _destination){
    this->destination = _destination;
}

void Command::setName(const std::string& _command_name){
    this->name = _command_name;
}

std::string Command::getName()const{
    return this->name;
}


} // endnamespace
