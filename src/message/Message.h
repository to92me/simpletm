#ifndef MSSG_H
#define MSSG_H

#include "Poco/String.h"


namespace core{

enum MessageType{
    COMMAND = 0,
    NOTIFICATION = 1,
    STOP_COMMAND = 3,
    COMMAND_RESPONSE = 4
};

class Message {
public:
    // constructors
    Message(MessageType _type, const std::string _sender):sender(_sender), type(_type){}
    Message(MessageType _type):sender("NONE"),type(_type){}
    Message(Message* _mssg):sender(_mssg->sender), type(_mssg->type){}

    virtual ~Message();

    virtual Message* clone()=0;


    std::string getSender() const;
    void setSender(const std::string& _sender);

    MessageType getType() const;
    void setType(const MessageType _type);

private:
    std::string sender;
    MessageType type;


};

} // end namespace
#endif
