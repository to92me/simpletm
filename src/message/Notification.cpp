#include "Notification.h"

namespace core {

std::string Notification::getTopic()const{
    return this->topic;
}

void Notification::setTopic(const std::string& _topic){
    this->topic = _topic;
}

}
