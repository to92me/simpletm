#ifndef STOP_COMMAND_H
#define STOP_COMMAND_H

#include "message/Message.h"

namespace core{

class StopCommand: public Message{
public:

    StopCommand(): Message(MessageType::STOP_COMMAND){}

    ~StopCommand(){}

    Message* clone(){return this;}

private:

};


}

#endif
