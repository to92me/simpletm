#ifndef COMMAND_RESPONSE_H
#define COMMAND_RESPONSE_H

#include "Command.h"
//#include "core/AbstractResponseInterface.h" // not needed and circular include

namespace core{

enum ResposeType{
    SUCCESS = 0,
    ERROR = 1,
    PROGRESS = 2
};


class CommandResponse: public Command {
public:

    CommandResponse(std::string _response_name, std::string _destination, std::string _sender, ResposeType _type):
        Command(_response_name,_destination,_sender), response_type(_type){ this->setType(MessageType::COMMAND_RESPONSE);}

    CommandResponse(CommandResponse *rsp):Command(rsp), response_type(rsp->getResponseType()){this->setType(MessageType::COMMAND_RESPONSE);}

    inline ResposeType getResponseType() const{
        return this->response_type;
    }

    inline void setResponseType(ResposeType _type){
        this->response_type = _type;
    }

protected:

private:
    ResposeType response_type;
};

}

#endif
