#include "Message.h"

namespace core {

std::string Message::getSender() const {
    return this->sender;
}

void Message::setSender(const std::string& _sender){
    this->sender = _sender;
}

void Message::setType(const MessageType _type){
    this->type = _type;
}

MessageType Message::getType()const {
    return this->type;
}

Message::~Message(){

}

}
