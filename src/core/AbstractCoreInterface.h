#ifndef ABSTRACT_CORE_INTERFACE_H
#define ABSTRACT_CORE_INTERFACE_H

#include "message/Message.h"

namespace core{

class AbstractCoreInterface{
public:
    virtual void receiveMessage(Message* _message)=0;
protected:

private:

};


}

#endif
