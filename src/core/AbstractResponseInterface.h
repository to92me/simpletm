#ifndef ABSTRACT_RESPONSE_INTERFACE_H
#define ABSTRACT_RESPONSE_INTERFACE_H

#include "message/CommandResponse.h"
#include "core/Logger.h"
//#include "utils/CallbacksContainer.h"
class CallbacksContainer;

/*
 * it could be skipped this class and used foreward of definitions but then code
 * would be hard to read and ... For better explanation read google syle quide class foreward definitions
 *
 */

namespace core{


class AbstractResponseInterface{
public:
    typedef void (AbstractResponseInterface::*messageCallback)(CommandResponse *_message);

protected:
    virtual void registerCommandCallbacks(const int _command_id,const messageCallback _sucess_callback, const messageCallback _error_callback,
                                  const messageCallback _progress_callback) = 0;

    // TODO this is bug it should be abstract but it not ... :/ then it is error and method is implemented do some research!
    virtual void registerCommandCallbacks(const int _command_id, CallbacksContainer *_container){ std::cout<< "Major ERROR :D "; }

    virtual messageCallback getCallback(const int _id, const ResposeType _type) = 0;

};

}

#endif
