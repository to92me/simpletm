#include "Node.h"

namespace core{


void Node::start(){
    this->thread.start(*this);
}



void Node::join(){
    this->thread.join();
}


}
