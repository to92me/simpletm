//----------------------------------------------------------
//
//Project:      Racel project
//Developer:    Tomislav Tumbas (to92me@gmail.com)
//mentor:       Ivan Mezei (imezei@uns.ac.rs)
//
//----------------------------------------------------------
//
//File: RobotMenu -- project main file
//
//
//----------------------------------------------------------
/* Description:
 *      here is task manager and all tasks instances
 *      Task manager will collect all tasks and run them
 *      on start of appllication
 *
*/
//----------------------------------------------------------

#ifndef NODE
#define NODE

#include "Poco/Message.h"
#include "Poco/Thread.h"


namespace core{

class Node{
public:
    virtual void init();
    virtual void start();
    virtual void stop();

    void join();

protected:
    virtual void main()=0;
    void setName();
}

}
#endif // NODE

