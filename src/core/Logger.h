#ifndef LOGGER_H
#define LOGGER_H

#include "Poco/Logger.h"
#include <iostream>
#include "Poco/Mutex.h"
#include "Poco/ScopedLock.h"

using std::string;
using Poco::FastMutex;
using Poco::ScopedLock;


namespace core{

class Logger {
private:
    Poco::Logger* logger;
    string name;
    Poco::FastMutex mutex;

public:

    Logger(string _name){
        this->logger = &Poco::Logger::get(_name);
        this->name = _name;
    }
    Logger(Logger* _logger){
        this->logger = &Poco::Logger::get(_logger->name);
        this->name = _logger->name;
    }

    void log(string _log);
    void debug(string _debug);
    void error(string _error);
    void info(string _info);

};
} //end namespace core

#endif
