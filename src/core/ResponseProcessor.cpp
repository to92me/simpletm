#include "ResponseProcessor.h"

namespace core{

void ResponseProcessor::registerCommandCallbacks(const int _command_id, const messageCallback _sucess_callback,
                                                 const messageCallback _error_callback, const messageCallback _progress_callback){

//    CallbacksContainer callbacks(_sucess_callback, _error_callback, _progress_callback);
//    respons_callbacks_map[_command_id] = &callbacks;
    CallbacksContainer *callbacks = new CallbacksContainer(_sucess_callback, _error_callback, _progress_callback);
    respons_callbacks_map[_command_id] = callbacks;

}

void ResponseProcessor::registerCommandCallbacks(const int _command_id, CallbacksContainer *_container){
    respons_callbacks_map[_command_id] = _container;
}

ResponseProcessor::messageCallback ResponseProcessor::getCallback(const int _id, const ResposeType _type){

    it = respons_callbacks_map.find(_id);
    if(it == respons_callbacks_map.end()){
        return NULL;

    }else{
        CallbacksContainer tmp;
        tmp = it->second;



        switch (_type) {
        case SUCCESS:{
            respons_callbacks_map.erase(it);
            return tmp.getSucess();
        }break;
        case ERROR:{
            respons_callbacks_map.erase(it);
            return tmp.getErrror();
        }break;

        case PROGRESS:{
            // if it is progress reponse do not delete id because we are waiting error or success reponse.
            return tmp.getProgress();
        }break;

        default:{
            return NULL;
        }break;

        }
    }
}

}// end namespace core
