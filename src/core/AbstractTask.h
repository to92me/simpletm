#ifndef ABSTRACT_TASKt_H
#define ABSTRACT_TASKt_H

#include "core/Node.h"
#include "core/MssgInterface.h"
#include "core/Logger.h"

using namespace core;

namespace core{

class AbstractTask: public Node, public MessageingInterface, public core::Logger{

public:
    AbstractTask(std::string _name): Node(_name), MessageingInterface(_name), Logger(_name){}
    AbstractTask(AbstractTask* _task): Node(_task), MessageingInterface(_task), Logger(_task){}

protected:

public:

};

}

#endif
