#ifndef NODE_H
#define NODE_H

#include "Poco/String.h"
#include "Poco/Thread.h"
#include "Poco/Runnable.h"
#include "Poco/RunnableAdapter.h"
#include <iostream>
#include "core/Logger.h"
using Poco::Thread;

namespace core{

class Node : public Poco::Runnable{
public:
    Node(const std::string _name): /*Logger(_name),*/ name(_name){}
    Node(const Node* _node): name(_node->name){}

    std::string getName() const{ return this->name;}

    void start();
    void join();

    virtual void init()=0;
    virtual void stop()=0;

protected:
    virtual void run()=0;
    Thread thread;

private:
    std::string name;



}; // end Node
} // end namespace

#endif
