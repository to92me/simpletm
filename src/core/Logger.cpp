#include "Logger.h"

namespace core{

void Logger::debug(string _debug){
  FastMutex::ScopedLock lock(mutex);

    std::cout  << this->name
               << "[ D ]: "
               << _debug
               << std::endl;

//        logger->debug(_debug);

}

void Logger::error(string _error){
  FastMutex::ScopedLock lock(mutex);

    std::cout << this->name
              << "[ E ]: "
              << _error
              << std::endl;



//        logger->error(_error);
}

void Logger::info(string _info){
   FastMutex::ScopedLock lock(mutex);

    std::cout  << this->name
               << "[ E ]: "
               << _info
               << std::endl;



//        logger->information(_info);
}


}
