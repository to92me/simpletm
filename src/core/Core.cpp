#include "Core.h"

namespace core{

void Core::AddTask(AbstractTask* _task ){
    task_map[_task->getName()]=_task;
    _task->setCore(this);
    std::stringstream ss;
    ss << "Starting task: "
       << _task->getName()
       << std::endl;

    //    info(ss.str());
    std::cout << ss.str();

}


void Core::initAllTasks(){

    for(std::map<std::string,AbstractTask*>::iterator it=task_map.begin(); it != task_map.end(); ++it){
        it->second->init();
    }
}

void Core::joinAllTasks(){

    for(std::map<std::string,AbstractTask*>::iterator it=task_map.begin(); it != task_map.end(); ++it){
        it->second->join();
    }

}

void Core::startAllTasks(){
    for(std::map<std::string,AbstractTask*>::iterator it=task_map.begin(); it != task_map.end(); ++it){
        it->second->start();
    }

}

void Core::receiveMessage(Message *_message){
    FastMutex::ScopedLock lock(mutex);

    message_queue.push(_message);
    condition.signal();
}

void Core::sendMessage(Message *_message){
    FastMutex::ScopedLock lock(mutex);

    _message->setSender("None");
    message_queue.push(_message);
    condition.signal();
}

void Core::run(){
    while (should_stop == false){


        if(message_queue.empty()){
            mutex.lock();
            condition.wait(mutex);
            mutex.unlock();
        }

        mutex.lock();
        Message* message = message_queue.front();
        message_queue.pop();
        mutex.unlock();

        switch (message->getType()) {
        case MessageType::COMMAND:
        {
            Command* command = (Command*) message;
            for(std::map<std::string,AbstractTask*>::iterator it=task_map.begin(); it != task_map.end(); ++it){
                if(it->first == command->getDestination()){
                    it->second->processCommand(command);
                }
            }


        }break;

        case MessageType::NOTIFICATION :
        {
            Notification* notification = (Notification*)message;
            for(std::map<std::string,AbstractTask*>::iterator it=task_map.begin(); it != task_map.end(); ++it){
                if(message->getSender()!= it->first &&
                        it->second->isSubscribed(notification->getTopic())){
                    it->second->processNotification(notification);
                }
            }
        }break;

        case MessageType::STOP_COMMAND :
        {
            for(std::map<std::string,AbstractTask*>::iterator it=task_map.begin(); it != task_map.end(); ++it){
                it->second->stop();
            }
            this->should_stop = true;
        }break;

        case MessageType::COMMAND_RESPONSE :
        {
            CommandResponse* command = (CommandResponse*) message;
            for(std::map<std::string,AbstractTask*>::iterator it=task_map.begin(); it != task_map.end(); ++it){
                if(it->first == command->getDestination()){
                    it->second->processCommandResponse(command);
                }
            }
        }break;

        default:
            break;
        }


    }

}

void Core::stopAllTasks(){
    for(std::map<std::string,AbstractTask*>::iterator it=task_map.begin(); it != task_map.end(); ++it){
        it->second->stop();
    }
}

void Core::stop(){
    // can not just set should_stop because thread is on conditonal wait
    StopCommand* stop_command = new StopCommand();
    this->sendMessage(stop_command);
}

} // end namespace
