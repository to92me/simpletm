#include "MssgInterface.h"


namespace core{

void MessageingInterface::registerCommand(const std::string _command_name, commandCallback _callback){
    command_registration[_command_name]=_callback;
}

void MessageingInterface::subscribe(const std::string _topic, notificationCallback _callback){
    topic_subscription[_topic]=_callback;
}

void MessageingInterface::setCore(AbstractCoreInterface *_core){
    this->core = _core;
}

bool MessageingInterface::isRegistread(std::string _command_name){
    auto search = command_registration.find(_command_name);
    if(search != command_registration.end()){
        return true;
    }else{
        return false;
    }
}

bool MessageingInterface::isSubscribed(std::string _topic){
    auto search = topic_subscription.find(_topic);
    if(search != topic_subscription.end()){
        return true;
    }else{
        return false;
    }
}

bool MessageingInterface::sendMessage(Message *_message){
    if(core != NULL){
        _message->setSender(this->name);
        core->receiveMessage(_message);
        return true;
    }else{
        return false;
    }
}

bool MessageingInterface::sendMessage(Command *_command, CallbacksContainer *_container){
    if(core != NULL){
        _command->setId(++id);
        this->registerCommandCallbacks(_command->getId(), _container);
        _command->setSender(this->name);
        core->receiveMessage((Message*)_command);
        return true;
    }else{
        return false;
    }
}

bool MessageingInterface::sendMessage(Command *_command, messageCallback _success, messageCallback _error, messageCallback _progress){
    if(core != NULL){
        _command->setId(++id);
        this->registerCommandCallbacks(_command->getId(), _success, _error, _progress);
        _command->setSender(this->name);
        core->receiveMessage(_command);
        return true;
    }else{
        return false;
    }
}

bool MessageingInterface::sendMessage(Command *_command, messageCallback _all_status){
    if(core != NULL){
        _command->setId(++id);
        this->registerCommandCallbacks(_command->getId(), _all_status,_all_status,_all_status);
        _command->setSender(this->name);
        core->receiveMessage(_command);
        return true;
    }else{
        return false;
    }
}

void MessageingInterface::processCommand(Command *_command){
    if(command_registration.find(_command->getName()) != command_registration.end()){
        (this->*command_registration[_command->getName()])(_command);
    }
}

void MessageingInterface::processNotification(Notification *_notification){
    if(topic_subscription.find(_notification->getTopic()) != topic_subscription.end()){
        (this->*topic_subscription[_notification->getTopic()])(_notification);
    }
}

void MessageingInterface::processCommandResponse(CommandResponse *_command_response){
    mssg_callback = this->getCallback(_command_response->getId(), _command_response->getResponseType());

    if(mssg_callback != NULL){
        (this->*mssg_callback)(_command_response);

    }else{
       std::cout << "WARNING: CommandResponse by id: "
                 <<  _command_response->getId()
                  << ", sender: "
                  << _command_response->getSender()
                  << ", destination: "
                  << _command_response->getDestination()
                  << " is not command with response options or already got SUCCESS or ERROR response."
                  << "This may be potential error"
                  << std::endl;
    }

}

bool MessageingInterface::sendResponse(Command *_command, ResposeType _type){
    if(this->core != NULL  && _command != NULL){
        CommandResponse *response = new CommandResponse((_command->getName()+ "_response"),_command->getSender(),this->name,_type);
        response->setId(_command->getId());
        core->receiveMessage(response);
        return true;
    }else{
        return false;
    }
}

bool MessageingInterface::sendResponse(Command *_command, CommandResponse *_response){
    if(this->core != NULL && _response != NULL && _command != NULL){
        _response->setDestination(_command->getSender());
        _response->setSender(this->name);
        _response->setId(_command->getId());
        core->receiveMessage(_response);
        return true;
    }else{
        return false;
    }
}

} // end namespace core
