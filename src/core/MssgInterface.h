#ifndef MESSAGEING_INTERFACE_H
#define MESSAGEING_INTERFACE_H

//#include "mssg/Command.h"
#include "message/Notification.h"
#include "message/Command.h"
#include "core/AbstractCoreInterface.h"
#include "core/ResponseProcessor.h"
#include "Poco/Mutex.h"

#include <map>

using Poco::FastMutex;

namespace core{

class MessageingInterface : public ResponseProcessor {
public:

    MessageingInterface(std::string _name):ResponseProcessor(_name), name(_name){}
    MessageingInterface(MessageingInterface* _interface): ResponseProcessor(_interface->name), name(_interface->name){}

    // calback for command registration
    typedef void (MessageingInterface::*commandCallback)(Command* _command);
    // callback for notification callback
    typedef void (MessageingInterface::*notificationCallback)(Notification* notification);

    void setCore(AbstractCoreInterface* _core);

    bool isSubscribed(const std::string _topic);
    bool isRegistread(const std::string _command_name);

    //methods that core calls to send commands and notifications to tasks
    void processCommand(Command* _command);
    void processNotification(Notification* _notification);
    void processCommandResponse(CommandResponse* _command_response);

protected:

    // regitration
    void registerCommand(const std::string _command_name, commandCallback _callback);
    void subscribe(const std::string _topic, notificationCallback _callback);

    // sending commands and notifications from tasks
    bool sendMessage(Message* _message);
    bool sendMessage(Command *_command, CallbacksContainer *_container);
    bool sendMessage(Command *_command, messageCallback _all_status);
    bool sendMessage(Command *_command, messageCallback _success, messageCallback _error, messageCallback _progress);

    bool sendResponse(Command *_command, ResposeType _type);
    bool sendResponse(Command *_command, CommandResponse *_response);


private:
    int id = 0;
    std::string name;
    AbstractCoreInterface* core;
    std::map<std::string, commandCallback> command_registration;
    std::map<std::string, notificationCallback> topic_subscription;
    messageCallback mssg_callback;
};
} // end namespace core

#endif // end of MESSAGEING_INTERFACE_H
