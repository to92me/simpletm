#ifndef CORE_H
#define CORE_H

#include "Poco/Mutex.h"
#include "Poco/Runnable.h"
#include "Poco/HashMap.h"
#include "Poco/String.h"
#include "Poco/ScopedLock.h"
#include "Poco/Condition.h"

#include <map>
#include "sstream"
#include <queue>

#include "core/AbstractTask.h"
#include "message/Message.h"
#include "core/AbstractCoreInterface.h"
#include "message/StopCommand.h"



using Poco::Mutex;
using Poco::FastMutex;
using Poco::ScopedLock;
using Poco::HashMap;
using Poco::Logger;
using Poco::Condition;
using std::map;

namespace core {

class Core: public Node, public AbstractCoreInterface{
public:
    Core():Node("Core"){}

    ~Core(){}

    void receiveMessage(Message *_message);

    void sendMessage(Message* _message);

    void AddTask(AbstractTask* _taks);

    // init and stop core
    void init(){}
    void stop();

    // methods to manage all tasks
    void initAllTasks();
    void startAllTasks();
    void stopAllTasks();

    // join all threads
    void joinAllTasks();

    //thread main method
    void run();

private:
    FastMutex mutex;
    Condition condition;
    map <std::string,AbstractTask*> task_map;
    bool should_stop = false;
    std::queue<Message*> message_queue;

}; // end class core
} // end nemespace

#endif // end CORE_H
