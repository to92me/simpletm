#ifndef RESPONSE_PROCESSOR_H
#define RESPONSE_PROCESSOR_H

#include <map>
#include "message/Message.h"
#include "AbstractResponseInterface.h"
#include "utils/CallbacksContainer.h"
#include <sstream>      // std::stringstream


namespace core{

class ResponseProcessor : public  AbstractResponseInterface{
public:
    ResponseProcessor(std::string _task_name)/*:Logger(_task_name)*/{}

protected:

    void registerCommandCallbacks(const int _command_id,const messageCallback _sucess_callback, const messageCallback _error_callback,
                                  const messageCallback _progress_callback);
    void registerCommandCallbacks(const int _command_id, CallbacksContainer *_container);

    messageCallback getCallback(const int _id, const ResposeType _type);



private:

    typedef std::map<int, CallbacksContainer*> ResponseCallbacksMapType;
    ResponseCallbacksMapType respons_callbacks_map;
    ResponseCallbacksMapType::iterator it;

};

}

#endif
