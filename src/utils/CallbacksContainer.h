#ifndef CALLBACKS_CONTAINER_H
#define CALLBACKS_CONTAINER_H

#include "core/AbstractResponseInterface.h"

namespace core{

class CallbacksContainer{
public:

    // must be defined because of std::map
//        pair<_T1, _T2>::
//        pair(tuple<_Args1...>& __tuple1, tuple<_Args2...>& __tuple2,
//         _Index_tuple<_Indexes1...>, _Index_tuple<_Indexes2...>)
//        : first(std::forward<_Args1>(std::get<_Indexes1>(__tuple1))...),
//          second(std::forward<_Args2>(std::get<_Indexes2>(__tuple2))...)
//        { }
    CallbacksContainer(){}
    CallbacksContainer(AbstractResponseInterface::messageCallback _success, AbstractResponseInterface::messageCallback _error,
                       AbstractResponseInterface::messageCallback _progress):
        success(_success), error(_error), progress(_progress){}
    CallbacksContainer(CallbacksContainer* _rc): success(_rc->getSucess()), error(_rc->getErrror()),
        progress(_rc->getProgress()){}

     AbstractResponseInterface::messageCallback getSucess(){ return this->success;}
     AbstractResponseInterface::messageCallback getErrror(){return this->error;}
     AbstractResponseInterface::messageCallback getProgress(){return this->progress;}

     void setSuccess(const AbstractResponseInterface::messageCallback _callback){ this->success = _callback; }
     void setError(const AbstractResponseInterface::messageCallback _callback){this->error = _callback;}
     void setProgress(const AbstractResponseInterface::messageCallback _callback){this->progress = _callback;}


private:

    AbstractResponseInterface::messageCallback success;
    AbstractResponseInterface::messageCallback error;
    AbstractResponseInterface::messageCallback progress;


};

}

#endif
